# README #

## Introduction:  

This is a HTML racing game which I initially created in 4 hours for a coding challenge. I later transformed this simple HTML game on the browser to a multiplayer game which can be played by two players from different browsers. It was a fun experience learning and playing around with new technologies.

View the application [here](https://unbounceracingame.herokuapp.com/)


## Technologies: ##

* HTML Canvas
* CSS
* NodeJs/ExpressJs
* Socket.io

## Purpose: ##

* Gain an understanding of Socket.io
* Gain an understanding of HTML Canvas

## Procedure: ##

1. Click [here](https://unbounceracingame.herokuapp.com/) to play the game.
2. Open this link again in another browser to act as player two.
3. Control the car using up, down, right and left arrow keys for player one. w, a, s, d keys for player two.



## Timestamp: ##

**Feb, 2016 – March, 2016