module.exports = function(express, app) {
	var router = express.Router();

	// =====================================
    // HOME PAGE
    // =====================================
	router.get('/', function(req, res){
		res.render('index', {
            
		});
	});

	// mount the router on the app
	// All routes relative to '/'
	app.use('/', router);
}
