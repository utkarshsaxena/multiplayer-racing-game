module.exports = function(io, myFirebaseRef) {   
    
    io.on('connection', function(socket){
        var playerStatus = {};
        var playerOne = false;
        var playerTwo = false;
        socket.on('beginGame', function(isKeyPressed){
                myFirebaseRef.once("value", function(snapshot) {            
                playerStatus = snapshot.val(); // Get player status from firebase

                // Assign player roles 
                if (! playerStatus.playerOne) {
                    playerOne = true;
                    myFirebaseRef.update({playerOne : true});
                    socket.join('playerOneRoom');
                    io.to('playerOneRoom').emit('assignedPlayerOne');               
                } else if (! playerStatus.playerTwo) {
                    myFirebaseRef.update({playerTwo : true});
                    playerTwo = true;
                    socket.join('playerTwoRoom');
                    io.to('playerTwoRoom').emit('assignedPlayerTwo');
                    io.emit('secondPlayerAdded');            
                } else {
                    socket.join('spectatorRoom');
                    io.to('spectatorRoom').emit('assignedSpectator');            
                }
            }, function (errorObject) {
                console.log("The read failed: " + errorObject.code);
            });
            
        });
                
        

        // Handle Key Events
        socket.on('rightKeyPressed', function(isKeyPressed){
            io.emit('rightKeyPressed', isKeyPressed);
        });

        socket.on('leftKeyPressed', function(isKeyPressed){
            io.emit('leftKeyPressed', isKeyPressed);
        });

        socket.on('upKeyPressed', function(isKeyPressed){
            io.emit('upKeyPressed', isKeyPressed);
        });

        socket.on('downKeyPressed', function(isKeyPressed){
            io.emit('downKeyPressed', isKeyPressed);
        });

        socket.on('aKeyPressed', function(isKeyPressed){
            io.emit('aKeyPressed', isKeyPressed);
        });

        socket.on('wKeyPressed', function(isKeyPressed){
            io.emit('wKeyPressed', isKeyPressed);
        });

        socket.on('sKeyPressed', function(isKeyPressed){
            io.emit('sKeyPressed', isKeyPressed);
        });

        socket.on('dKeyPressed', function(isKeyPressed){
            io.emit('dKeyPressed', isKeyPressed);
        });        
        // END    

        socket.on('disconnect', function(){                
            if (playerOne) {
                myFirebaseRef.update({playerOne : false});
                console.log('Player one disconnected');                
            } else if (playerTwo) {
                myFirebaseRef.update({playerTwo : false});                
            }
        });
    });
}
