'use strict'
var socket = io();
var beginGame;
var unbounceGame = function () {
    // Initialize Variables
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    var canvasHeight = canvas.height;
    var canvasWidth = canvas.width;
    var trackHeight = canvasHeight/2;
    var trackWidth = canvasWidth;
    var trackY = canvasWidth/8;
    var trackX = 0;
    var carWidth = 100;
    var carHeight = 50;
    var carOne = {
        x: trackX + 10,
        y: trackY + 40,
        img: new Image()
    };
    var carTwo = {
        x: trackX + 10,
        y: trackY + 220,
         img: new Image()
    };
    var rightKeyPressed = false;
    var leftKeyPressed = false;
    var upKeyPressed = false;
    var downKeyPressed = false;
    var dKeyPressed = false;
    var aKeyPressed = false;
    var wKeyPressed = false;
    var sKeyPressed = false;
    var raceStarted = false;
    var count = 3;
    var playerOne = false;
    var playerTwo = false;
    var spectator = false;
    var secondPlayerExists = false;
    
    return {
        canvas : canvas,
        ctx : ctx,
        canvasHeight : canvasHeight,
        canvasWidth : canvasWidth,
        trackHeight : trackHeight,
        trackWidth : trackWidth,
        trackY : trackY,
        trackX : trackX,
        carWidth : carWidth,
        carHeight : carHeight,
        carOne : carOne,
        carTwo : carTwo,
        rightKeyPressed : rightKeyPressed,
        leftKeyPressed : leftKeyPressed,
        upKeyPressed : upKeyPressed,
        downKeyPressed : downKeyPressed,
        dKeyPressed : dKeyPressed,
        aKeyPressed : aKeyPressed,
        wKeyPressed : wKeyPressed,
        sKeyPressed : sKeyPressed,
        raceStarted : raceStarted,
        count : count,
        playerOne : playerOne,
        playerTwo : playerTwo,
        spectator : spectator,
        secondPlayerExists : secondPlayerExists
    }
}();

function changeHeading(headingText) {
    unbounceGame.ctx.clearRect(0,0,unbounceGame.canvas.width,unbounceGame.canvas.height);
    unbounceGame.ctx.font="45px Arial";
    unbounceGame.ctx.fillStyle = "#E53935";
    unbounceGame.ctx.textAlign = "center";
    unbounceGame.ctx.fillText(headingText, unbounceGame.canvas.width/2, unbounceGame.canvas.height/2); 
}

document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

function keyDownHandler(event) {
    
    if(event.keyCode === 13 && ! unbounceGame.raceStarted) {
        socket.emit('beginGame', true);        
    }    
  
  if(event.keyCode === 39 && unbounceGame.playerOne) {
      socket.emit('rightKeyPressed', true);
      unbounceGame.rightKeyPressed = true;        
  }

  if(event.keyCode === 37 && unbounceGame.playerOne) {
      socket.emit('leftKeyPressed', true);
      unbounceGame.leftKeyPressed  = true;        
  }
  
  if(event.keyCode === 38 && unbounceGame.playerOne && (unbounceGame.rightKeyPressed || unbounceGame.leftKeyPressed)) {
    socket.emit('upKeyPressed', true);
    unbounceGame.upKeyPressed = true;        
  }

  if(event.keyCode === 40 && unbounceGame.playerOne && (unbounceGame.rightKeyPressed || unbounceGame.leftKeyPressed)) {
    socket.emit('downKeyPressed', true);
    unbounceGame.downKeyPressed  = true;        
  }
  
  if(event.keyCode === 68 && unbounceGame.playerTwo) {
    socket.emit('dKeyPressed', true);
    unbounceGame.dKeyPressed = true;        
  }

  if(event.keyCode === 65 && unbounceGame.playerTwo) {
    socket.emit('aKeyPressed', true);
    unbounceGame.aKeyPressed  = true;        
  }
  
  if(event.keyCode === 87 && unbounceGame.playerTwo && (unbounceGame.dKeyPressed || unbounceGame.aKeyPressed)) {
    socket.emit('wKeyPressed', true);
    unbounceGame.wKeyPressed = true;        
  }

  if(event.keyCode === 83 && unbounceGame.playerTwo && (unbounceGame.dKeyPressed || unbounceGame.aKeyPressed)) {
    socket.emit('sKeyPressed', true);
    unbounceGame.sKeyPressed  = true;        
  }
}

function keyUpHandler(event) {
  if(event.keyCode === 39 && unbounceGame.playerOne) {
    socket.emit('rightKeyPressed', false);
    unbounceGame.rightKeyPressed = false;        
  }

  if(event.keyCode === 37 && unbounceGame.playerOne) {
    socket.emit('leftKeyPressed', false);
    unbounceGame.leftKeyPressed  = false;        
  }
  
  if(event.keyCode === 38 && unbounceGame.playerOne) {
    socket.emit('upKeyPressed', false);
    unbounceGame.upKeyPressed = false;        
  }

  if(event.keyCode === 40 && unbounceGame.playerOne) {
    socket.emit('downKeyPressed', false);
    unbounceGame.downKeyPressed  = false;        
  }
  
  if(event.keyCode === 68 && unbounceGame.playerTwo) {
    socket.emit('dKeyPressed', false);
    unbounceGame.dKeyPressed = false;        
  }

  if(event.keyCode === 65 && unbounceGame.playerTwo) {
    socket.emit('aKeyPressed', false);
    unbounceGame.aKeyPressed  = false;        
  }
  
  if(event.keyCode === 87 && unbounceGame.playerTwo) {
    socket.emit('wKeyPressed', false);
    unbounceGame.wKeyPressed = false;        
  }

  if(event.keyCode === 83 && unbounceGame.playerTwo) {
    socket.emit('sKeyPressed', false);
    unbounceGame.sKeyPressed  = false;        
  }
};

socket.on('assignedPlayerOne', function(id){
    unbounceGame.playerOne = true;
    changeHeading("Waiting for player two.")
    console.log('Assigned Player One ');
});

socket.on('assignedPlayerTwo', function(){
    unbounceGame.playerTwo = true;
    console.log('Assigned Player Two');
});

socket.on('assignedSpectator', function(){
    unbounceGame.spectator = true;
    alert('There are already two player in this game. Please try again later.');
});

socket.on('secondPlayerAdded', function(){
    unbounceGame.secondPlayerExists = true;
    beginGame =  setInterval(function(){countDown(unbounceGame.ctx,unbounceGame.count--);},1000);
});

socket.on('rightKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.rightKeyPressed  = true;        
    } else {
        unbounceGame.rightKeyPressed  = false;        
    }
});

socket.on('leftKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.leftKeyPressed  = true;        
    } else {
        unbounceGame.leftKeyPressed  = false;        
    }
});

socket.on('upKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.upKeyPressed  = true;        
    } else {
        unbounceGame.upKeyPressed  = false;        
    }
});

socket.on('downKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.downKeyPressed  = true;        
    } else {
        unbounceGame.downKeyPressed  = false;        
    }
});

socket.on('aKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.aKeyPressed  = true;        
    } else {
        unbounceGame.aKeyPressed  = false;        
    }
});

socket.on('wKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.wKeyPressed  = true;        
    } else {
        unbounceGame.wKeyPressed  = false;        
    }
});

socket.on('sKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.sKeyPressed  = true;        
    } else {
        unbounceGame.sKeyPressed  = false;        
    }
});

socket.on('dKeyPressed', function(isKeyPressed){
    if (isKeyPressed) {
        unbounceGame.dKeyPressed  = true;        
    } else {
        unbounceGame.dKeyPressed  = false;        
    }
});

// Draw the car component
unbounceGame.carOne.img.src = "images/player-one-car.png";
function drawCarOne(x, y) {  
  unbounceGame.ctx.drawImage(unbounceGame.carOne.img, x, y);
}

// Draw the car component
unbounceGame.carTwo.img.src = "images/player-two-car.png";
function drawCarTwo(x, y) {
  unbounceGame.ctx.drawImage(unbounceGame.carTwo.img, x, y);
}

// Draw the racetrack component
function drawRaceTrack() {
  unbounceGame.ctx.beginPath();
  unbounceGame.ctx.rect(unbounceGame.trackX, unbounceGame.trackY, unbounceGame.trackWidth, unbounceGame.trackHeight);
  unbounceGame.ctx.fillStyle = "#4f4f4f";
  unbounceGame.ctx.fill();
  unbounceGame.ctx.closePath();
  for(var i=0, l=15; i<l; i++) {
    unbounceGame.ctx.beginPath();
    unbounceGame.ctx.rect(100 * i, 2 * unbounceGame.trackY, 70, 10);
    unbounceGame.ctx.fillStyle = "white";
    unbounceGame.ctx.fill();
    unbounceGame.ctx.closePath();
  }
  // Finish line
  unbounceGame.ctx.beginPath();
  unbounceGame.ctx.moveTo(unbounceGame.canvas.width - unbounceGame.carWidth - 20, unbounceGame.trackY);
  unbounceGame.ctx.lineTo(unbounceGame.canvas.width - unbounceGame.carWidth - 20, unbounceGame.trackY + unbounceGame.trackHeight);
  unbounceGame.ctx.lineWidth = 5;
  unbounceGame.ctx.strokeStyle = "red";
  unbounceGame.ctx.stroke();
  unbounceGame.ctx.closePath();
  
  // Start line
  unbounceGame.ctx.beginPath();
  unbounceGame.ctx.moveTo(unbounceGame.trackX + 10 + unbounceGame.carWidth, unbounceGame.trackY);
  unbounceGame.ctx.lineTo(unbounceGame.trackX + 10 + unbounceGame.carWidth, unbounceGame.trackY + unbounceGame.trackHeight);
  unbounceGame.ctx.lineWidth = 5;
  unbounceGame.ctx.strokeStyle = "green";
  unbounceGame.ctx.stroke();
  unbounceGame.ctx.closePath();
}

// Detect collision between cars
function detectCollision() {
  // Handle collision of car two with car one
  if (unbounceGame.carOne.x < unbounceGame.carTwo.x + unbounceGame.carWidth - 10 &&
      unbounceGame.carOne.x + unbounceGame.carWidth - 10 > unbounceGame.carTwo.x &&
      unbounceGame.carOne.y < unbounceGame.carTwo.y + unbounceGame.carHeight - 10 &&
      unbounceGame.carHeight - 10 + unbounceGame.carOne.y > unbounceGame.carTwo.y) {
    changeHeading('Cars met with an accident. Restarting the race.');
    setTimeout(function() {
        document.location.reload();
    }, 3000);
  }
}

// Player One Control Logic
function playerOneControlLogic() {
    if (unbounceGame.rightKeyPressed && unbounceGame.carOne.x < unbounceGame.canvas.width - unbounceGame.carWidth) {
      unbounceGame.carOne.x += 4;
    }
    if (unbounceGame.leftKeyPressed && unbounceGame.carOne.x > 0) {
      unbounceGame.carOne.x -= 4;
    }
    if (unbounceGame.upKeyPressed && unbounceGame.carOne.y > unbounceGame.trackY) {
      unbounceGame.carOne.y -= 4;
    }
    if (unbounceGame.downKeyPressed && unbounceGame.carOne.y < unbounceGame.trackY + unbounceGame.trackHeight - unbounceGame.carHeight) {
      unbounceGame.carOne.y += 4;
    }
};

// Player Two Control Logic
function playerTwoControlLogic() {
    if (unbounceGame.dKeyPressed && unbounceGame.carTwo.x < unbounceGame.canvas.width - unbounceGame.carWidth) {
      unbounceGame.carTwo.x += 4;
    }
    if (unbounceGame.aKeyPressed && unbounceGame.carTwo.x > 0) {
      unbounceGame.carTwo.x -= 4;
    }
    if (unbounceGame.wKeyPressed && unbounceGame.carTwo.y > unbounceGame.trackY) {
      unbounceGame.carTwo.y -= 4;
    }
    if (unbounceGame.sKeyPressed && unbounceGame.carTwo.y < unbounceGame.trackY + unbounceGame.trackHeight - unbounceGame.carHeight) {
      unbounceGame.carTwo.y += 4;
    }
};

// End of the race resules
function raceResultLogic() {
    // End of ther race results
    if ((unbounceGame.carOne.x > unbounceGame.canvas.width - unbounceGame.carWidth - 20) && (unbounceGame.carTwo.x > unbounceGame.canvas.width - unbounceGame.carWidth - 20)) {
        changeHeading('It is a draw. Starting a new race.');
        setTimeout(function() {
            document.location.reload();
        }, 2000);      
    } else if (unbounceGame.carOne.x > unbounceGame.canvas.width - unbounceGame.carWidth - 20) {
      changeHeading('Player One wins. Starting a new race.');
        setTimeout(function() {
            document.location.reload();
        }, 2000);
    } else if (unbounceGame.carTwo.x > unbounceGame.canvas.width - unbounceGame.carWidth - 20) {
      changeHeading('Player Two wins. Starting a new race.');
        setTimeout(function() {
            document.location.reload();
        }, 2000);
    }
};

// Redner all game components
function renderGame() {
  // Clear canvas before rendering
  unbounceGame.ctx.clearRect(0, 0, unbounceGame.canvas.width, unbounceGame.canvas.height);
  
  // Draw components
  drawRaceTrack();
  drawCarOne(unbounceGame.carOne.x, unbounceGame.carOne.y);
  drawCarTwo(unbounceGame.carTwo.x, unbounceGame.carTwo.y);
  detectCollision();
  
  if (unbounceGame.raceStarted) {
      playerOneControlLogic();
      playerTwoControlLogic();
      raceResultLogic();
    }  
  requestAnimationFrame(renderGame);    
}

// Count down before the race begins
function countDown(ctx, count) {
  changeHeading("Race starts in.." + count);
  if(count === 0){
    clearInterval(beginGame);
    unbounceGame.raceStarted = true;
    renderGame();   
  }
}

changeHeading('Press Enter key to begin game');
