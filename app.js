// Initialize all modules
var express = require('express');
var app = express();
var cons = require('consolidate');
var path = require('path');
var swig = require('swig');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Firebase = require('firebase');
var myFirebaseRef = new Firebase('https://unbounce-racing.firebaseio.com/');
var port = process.env.PORT || 4000;

// Register our templating engine
app.engine('html', cons.swig);
app.set('view engine', 'html');
app.set('views', __dirname + '/html');

// Express middleware to serve static files
app.use(express.static(__dirname + '/assets'));

// Get routes file(s)
require('./routes/main.js')(express, app);


require('./socketio/serverSocket.js')(io, myFirebaseRef);

http.listen(port, function() {
    console.log('Node app is running on port', port);
});	